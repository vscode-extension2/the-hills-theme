# [The hills](https://marketplace.visualstudio.com/items?itemName=HaikelFazzani.the-hills)
based on vscode dark theme

## Captures
![](https://gitlab.com/vscode-extension2/the-hills-theme/-/raw/master/the-hills.png?inline=false)

**Enjoy!**

# License
MIT
